Docker
======

Development run command::

    docker build -t ostbay-backend-files-service .
    docker run -p 8000:80 -v $(pwd):/app ostbay-backend-files-service /start-reload.sh


Docker image docs::

    https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker



Docker-compose::

    docker-compose build
    docker-compose up


Local
=====

Poetry::

    poetry install
    poetry run uvicorn app.main:app  --reload


Deploy
======

1. Rename .env_template to .env
2. Copy to ./app/.env
3. Up with docker-compose or docker
