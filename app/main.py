from functools import lru_cache
from logging import getLogger
from typing import List

import boto3
from botocore.exceptions import ClientError
from fastapi import Depends, FastAPI, File, Form, HTTPException, UploadFile
from firebase_admin import credentials, firestore, auth, initialize_app
from google.cloud.firestore_v1.proto.firestore_pb2_grpc import Firestore
from starlette.middleware.cors import CORSMiddleware

from app.config import Settings


log = getLogger(__name__)
app = FastAPI()


@lru_cache
def get_settings() -> Settings:
    return Settings()


@lru_cache()
def get_firebase_db() -> Firestore:
    firebase_credentials = credentials.Certificate(get_settings().firebase_credentials)
    firebase_app = initialize_app(firebase_credentials)
    return firestore.client(firebase_app)


@lru_cache()
def get_s3_client() -> boto3.Session:
    return boto3.client("s3", **get_settings().s3_credentials)


app.add_middleware(CORSMiddleware, allow_origins=get_settings().CORS_ORIGINS)


@app.post(
    path="/upload/",
    responses={
        200: {
            "content": {
                "application/json": {
                    "example": {"detail": "ok"},
                },
            },
        },
    },
)
def upload(
    firebase_token: str = Form(...),
    project_id: str = Form(...),
    files: List[UploadFile] = File(...),
    firebase_db: Firestore = Depends(get_firebase_db),
    s3_client: boto3.Session = Depends(get_s3_client),
    settings: Settings = Depends(get_settings),
) -> dict:
    """
    Uploads one or multiple files to S3 bucket.
    """
    try:
        decoded_token = auth.verify_id_token(firebase_token)
    except (auth.ExpiredIdTokenError, auth.InvalidIdTokenError) as e:
        log.error(f"[Upload] Error occurred while files upload on token verify: {e}")
        raise HTTPException(
            status_code=401,
            detail="firebase_token is invalid or expired",
        )
    else:
        # Iterate through each file and upload if it does not exist
        for file in files:
            s3_file_path = f"{decoded_token['user_id']}/{file.filename}"

            # Upload file if it does not exist
            try:
                s3_client.head_object(Bucket=settings.AWS_S3_BUCKET, Key=s3_file_path)
            except ClientError as e:
                if e.response.get("Error") == {'Code': '404', 'Message': 'Not Found'}:
                    log.info(f"[Upload] file does not exist in S3, uploading...")
                    s3_client.upload_fileobj(file.file, "ostbay-files", s3_file_path)
                else:
                    log.error(f"[Upload] unexpected error occurred: {e}")

            # Check if firebase document exist and create it if it doesn't
            if not (
                firebase_db.collection("files")
                .where("filename", "==", file.filename)
                .where("user_id", "==", decoded_token["user_id"])
                .where("project_id", "==", project_id)
                .get()
            ):
                firebase_db.collection("files").document().set({
                    "user_id": decoded_token["user_id"],
                    "project_id": project_id,
                    "filename": file.filename,
                })

        # Get all user objects from S3 by uid prefix for a folder
        total_files_size_in_bytes = 0
        response = s3_client.list_objects(
            Bucket=settings.AWS_S3_BUCKET,
            Prefix=decoded_token["user_id"],
        )

        # Calculate total files size in bytes
        for obj_data in response["Contents"]:
            total_files_size_in_bytes += obj_data["Size"]

        # Update total files size in firebase
        firebase_db.collection("profiles").document(decoded_token["email"]).set({
            "total_files_size_in_bytes": total_files_size_in_bytes,
        })

        return {"detail": "ok"}
